function down(event) {
    if(~event.target.className.search(/drag/)){
      dragObj = makeObj(event);
          document.addEventListener("mousemove", freeMovement);
  }
  }
  function freeMovement(event) {
      document.addEventListener("mouseup", drop);
      dragObj.element.style.left = Math.max(dragObj.minBoundX, Math.min(event.clientX - dragObj.posX, dragObj.maxBoundX)) + "px";
      dragObj.element.style.top = Math.max(dragObj.minBoundY, Math.min(event.clientY - dragObj.posY, dragObj.maxBoundY)) + "px";
  }
  function drop() {
      document.removeEventListener("mousemove", freeMovement);
  }
  function makeObj(event) {
      var obj = new Object(),
      e = event.target; 
      obj.element = e;
      obj.minBoundX =0;
      obj.minBoundY =0;
      obj.maxBoundX =  e.parentNode.offsetWidth -e.offsetWidth;
      obj.maxBoundY =  e.parentNode.offsetHeight -e.offsetHeight;
      obj.posX = event.clientX - e.offsetLeft;
      obj.posY = event.clientY - e.offsetTop;
      return obj;
  }
  document.addEventListener("mousedown", down);
  
  function center(){
      $(".drag").addClass("dragCenter");
    }
  function lRight(){
    $(".drag").addClass("dragLowerRight");
    $(".drag").removeClass("dragCenter");
  }  

  $('.center').click(function(){
    center();
  })
  $('.lRight').click(function(){
      lRight();
  })



  